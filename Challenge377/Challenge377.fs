﻿module Fitter

let fit1 crateX crateY boxX boxY = (crateX / boxX) * (crateY / boxY)


let fit2 crateX crateY boxX boxY = List.max [
        fit1 crateX crateY boxX boxY
        fit1 crateX crateY boxY boxX
    ]


let fit3 crateX crateY crateZ boxX boxY boxZ = List.max [
         (crateX / boxX) * (crateY / boxY) * (crateZ / boxZ)
         (crateX / boxY) * (crateY / boxX) * (crateZ / boxZ)
         (crateX / boxZ) * (crateY / boxY) * (crateZ / boxX)
         (crateX / boxY) * (crateY / boxZ) * (crateZ / boxX)
         (crateX / boxX) * (crateY / boxZ) * (crateZ / boxY)
         (crateX / boxZ) * (crateY / boxX) * (crateZ / boxY)
    ]