﻿module Challenge377Tests

open Challenge377
open NUnit.Framework


[<Test>]
let fit1() = 
    Assert.AreEqual(12,   Fitter.fit1 25 18 6 5)
    Assert.AreEqual(100,  Fitter.fit1 10 10 1 1)
    Assert.AreEqual(10,   Fitter.fit1 12 34 5 6)
    Assert.AreEqual(5676, Fitter.fit1 12345 678910 1112 1314)
    Assert.AreEqual(0,    Fitter.fit1 5 100 6 1)


[<Test>]
let fit2() =
    Assert.AreEqual(15,   Fitter.fit2 25 18 6 5)
    Assert.AreEqual(12,   Fitter.fit2 12 34 5 6)
    Assert.AreEqual(5676, Fitter.fit2 12345 678910 1112 1314)
    Assert.AreEqual(80,   Fitter.fit2 5 100 6 1)
    Assert.AreEqual(0,    Fitter.fit2 5 5 6 1)


[<Test>]
let fit3() =
    Assert.AreEqual(1000,   Fitter.fit3 10 10 10 1 1 1)
    Assert.AreEqual(32,     Fitter.fit3 12 34 56 7 8 9)
    Assert.AreEqual(32604,  Fitter.fit3 123 456 789 10 11 12)
    Assert.AreEqual(174648, Fitter.fit3 1234567 89101112 13141516 171819 202122 232425)