﻿module MainModule

// helper to show solution
let showResults prob solution = printfn "Prob: %A\nSolution: %A" prob solution

// solver method, list deconstruct and pass arguments to module method
let solve1 = function
    | x1::y1::x2::y2::_ -> Fitter.fit1 x1 y1 x2 y2
    | _ -> 0


[<EntryPoint>]
let main argv = 
    // first pattern test (Tests have been created, so no need of that)
    showResults [25; 18; 6; 5] (solve1 [25; 18; 6; 5]) |> ignore

    0 // return an integer exit code
