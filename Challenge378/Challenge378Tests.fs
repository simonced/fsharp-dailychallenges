﻿module Challenge378Tests

open NUnit.Framework
open Problem378

(*
[5; 3; 0; 2; 6; 2; 0; 7; 2; 5] // should be false
[4; 2; 0; 1; 5; 0] // should be false
[3; 1; 2; 3; 1; 0] // should be true
[16; 9; 9; 15; 9; 7; 9; 11; 17; 11; 4; 9; 12; 14; 14; 12; 17; 0; 3; 16] // should be true
[14; 10; 17; 13; 4; 8; 6; 7; 13; 13; 17; 18; 8; 17; 2; 14; 6; 4; 7; 12] // should be true
[15; 18; 6; 13; 12; 4; 4; 14; 1; 6; 18; 2; 6; 16; 0; 9; 10; 7; 12; 3] // should be false
[6; 0; 10; 10; 10; 5; 8; 3; 0; 14; 16; 2; 13; 1; 2; 13; 6; 15; 5; 1] // should be false
[2; 2; 0] // false
[3; 2; 1] // false
[1; 1] // true
[1] // false
[] // true
*)

[<Test>]
let testSolve() =
    Assert.IsFalse ( Problem378.solve [5; 3; 0; 2; 6; 2; 0; 7; 2; 5] )
    Assert.IsFalse ( Problem378.solve [4; 2; 0; 1; 5; 0] )
    Assert.IsTrue  ( Problem378.solve [3; 1; 2; 3; 1; 0] )
    Assert.IsTrue  ( Problem378.solve [16; 9; 9; 15; 9; 7; 9; 11; 17; 11; 4; 9; 12; 14; 14; 12; 17; 0; 3; 16] )
    Assert.IsTrue  ( Problem378.solve [14; 10; 17; 13; 4; 8; 6; 7; 13; 13; 17; 18; 8; 17; 2; 14; 6; 4; 7; 12] )
    Assert.IsFalse ( Problem378.solve [15; 18; 6; 13; 12; 4; 4; 14; 1; 6; 18; 2; 6; 16; 0; 9; 10; 7; 12; 3] )
    Assert.IsFalse ( Problem378.solve [6; 0; 10; 10; 10; 5; 8; 3; 0; 14; 16; 2; 13; 1; 2; 13; 6; 15; 5; 1] )
    Assert.IsFalse ( Problem378.solve [2; 2; 0] )
    Assert.IsFalse ( Problem378.solve [3; 2; 1] )
    Assert.IsTrue  ( Problem378.solve [1; 1] )
    Assert.IsFalse ( Problem378.solve [1] )
    Assert.IsTrue  ( Problem378.solve [] )