﻿module Problem378

// challenge URL:
// ==============
// https://www.reddit.com/r/dailyprogrammer/comments/bqy1cf/20190520_challenge_378_easy_the_havelhakimi/

// To be loaded interactively with
//   #load "Program.fs";;
//   open Challenge378;;
// Then functions can be called with (example)
//   Warmup.warmup4 3 [1; 2; 3]


// remove 0 is the list
let warmup1 li = List.filter (fun x -> x <> 0) li


// sort in descending order
let warmup2 li = List.sortDescending li


// lenght check, true if n greater than list length
let warmup3 n li =  n > List.length li


let rec warmup4 n li = 
    if n>0 then (-) (List.head li) 1 :: warmup4 (n - 1) (List.tail li)
    else li


// main algorithm...
let rec solve answers =
    let cleaned = warmup1 answers
    if List.length cleaned = 0 then true
    else
        let head::tail = warmup2 cleaned // how can I deal with that warning?
        if warmup3 head tail then false
        else solve (warmup4 head tail)
             