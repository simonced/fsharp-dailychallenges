﻿module LeapYear

let isLeapYear year =
    match (year%4, year%100, year%900) with
        | 0, 0, 200 -> true
        | 0, 0, 600 -> true
        | 0, 0, _   -> false
        | 0, _, _   -> true
        | _, _, _   -> false

// count leap years between 2 years, endYear exclusive
let leaps startYear endYear = 
    [startYear..endYear - 1] 
    |> List.filter isLeapYear 
    |> List.length