﻿module Challenge376Tests

open NUnit.Framework
open LeapYear

[<Test>]
let isLeapYear() =
    Assert.IsTrue  (LeapYear.isLeapYear 2000)
    Assert.IsFalse (LeapYear.isLeapYear 1999)
    Assert.IsTrue  (LeapYear.isLeapYear 1960) // true
    Assert.IsFalse (LeapYear.isLeapYear 1981) // false
    Assert.IsTrue  (LeapYear.isLeapYear 1984) // true


[<Test>]
let leaps() =
    Assert.AreEqual( 29,      LeapYear.leaps 1900 2020)
    Assert.AreEqual( 1,       LeapYear.leaps 2016 2017)
    Assert.AreEqual( 0,       LeapYear.leaps 2019 2020)
    Assert.AreEqual( 0,       LeapYear.leaps 1900 1901)
    Assert.AreEqual( 1,       LeapYear.leaps 2000 2001)
    Assert.AreEqual( 0,       LeapYear.leaps 2800 2801)
    Assert.AreEqual( 0,       LeapYear.leaps 123456 123456)
    Assert.AreEqual( 1077,    LeapYear.leaps 1234 5678)
    Assert.AreEqual( 1881475, LeapYear.leaps 123456 7891011)
    // big boss (have to handle numbers bigger than 32bit)
    // this is how to write the bigintegers, but my functions need to be reworked to handle those,
    // not interested in doing so right now.
    //Assert.AreEqual(288412747246240I, LeapYear.leaps 123456789101112I 1314151617181920I)

(*
TODO testing leap years count

leaps(1900, 2020) => 29 // found online as suplementary example

leaps(2016, 2017) => 1
leaps(2019, 2020) => 0
leaps(1900, 1901) => 0
leaps(2000, 2001) => 1
leaps(2800, 2801) => 0
leaps(123456, 123456) => 0
leaps(1234, 5678) => 1077
leaps(123456, 7891011) => 1881475
*)
